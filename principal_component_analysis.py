# -*- coding: utf-8 -*-
u"""主成分分析."""
import numpy as np


class PCA(object):
	u"""主成分分析の実装."""

	def __init__(self, n_components=None):
		u"""初期化."""
		self.lam = None          # 固有値
		self.transMat = None     # 固有ベクトルを並べた変換行列
		self.ncomp = n_components        # 圧縮した後の次元数
		self.mu = None           # 訓練データの平均

	def fit(self, trainX):
		u"""最適化問題を解く."""
		trainX = np.array(trainX)
		n, d = trainX.shape
		if self.ncomp is None:
			self.ncomp = d

		self.mu = np.mean(trainX, axis=0)
		trainX -= self.mu[np.newaxis, :]    # 平均を引いておく

		sigma = np.dot(trainX.T, trainX) / n   # 共分散行列
		w, v = np.linalg.eigh(sigma)     # w: 固有値(昇順), v: 固有ベクトル

		self.lam = w
		self.transMat = v

	def transform(self, X):
		u"""次元削減する."""
		Xb = X - self.mu[np.newaxis, :]
		transX = np.dot(Xb, self.transMat[:, -self.ncomp:])
		return transX

























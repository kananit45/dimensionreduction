# -*- coding: utf-8 -*-
u"""線形判別分析."""
import numpy as np


class LDA(object):
	u"""線形判別分析の実装."""

	def __init__(self, n_components=None):
		u"""初期化."""
		self.lam = None          # 固有値
		self.transMat = None     # 固有ベクトルを並べた変換行列
		self.ncomp = n_components        # 圧縮した後の次元数
		self.mu1 = None           # クラス1の平均
		self.mu2 = None           # クラス2の平均

	def fit(self, X1, X2):
		u"""
		最適化問題を解く.

		引数
			X1 : クラス1の特徴
			X2 : クラス2の特徴
		"""
		n1, d = X1.shape
		n2, _ = X2.shape
		if self.ncomp is None:
			self.ncomp = d

		mu1 = np.mean(X1, axis=0)
		mu2 = np.mean(X2, axis=0)
		Sb = np.outer(mu1 - mu2, mu1 - mu2)     # クラス間変動
		Sw = np.dot((X1 - mu1).T, X1 - mu1)\
			+ np.dot((X2 - mu2).T, X2 - mu2)        # クラス内変動

		w, v = np.linalg.eig(np.linalg.solve(Sw, Sb))  # 固有値問題を解く
		ascent = np.argsort(w)
		w = w[ascent]
		v = v[:, ascent]

		self.mu1 = mu1
		self.mu2 = mu2
		self.lam = w
		self.transMat = v

	def transform(self, X):
		u"""次元削減する."""
		transX = np.dot(X, self.transMat[:, -self.ncomp:])
		return transX

